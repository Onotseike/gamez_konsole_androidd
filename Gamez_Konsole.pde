import apwidgets.*;

import controlP5.*;
import apwidgets.*;
import ketai.ui.*;

//Objects
Mem Memory;
Ayo ayo;
ArrayList<Misil> misiles;
Nave nave;

KetaiVibrate vibe;
ControlP5 cp5;
APMediaPlayer player, playa,drum;


//variables
boolean started = false;
boolean mem_inplay = false;
boolean space_inplay = false;
boolean ayo_inplay = false;
PImage bck, logo, about, help,aid,info;


float velocidad;
int puntos;
int vidas;
int bonus = 40;
int contBonus = 0;
int atractor;
boolean tokenVida;
boolean tokenAtractor;



void setup()
{
  //size(displayWidth, displayHeight);
//  orientation(LANDSCAPE);
  vibe = new KetaiVibrate(this);
  cp5 = new ControlP5(this);
  player = new APMediaPlayer(this);
  player.setMediaFile("soundtrack1.mp3");
  playa = new APMediaPlayer(this);
  playa.setMediaFile("soundtrack2.mp3");
  drum = new APMediaPlayer(this);
  drum.setMediaFile("drum.mp3");

  Memory = new Mem(int(displayWidth/5), int(displayHeight/5)); 
  


  bck = loadImage("Bck.png");
  logo = loadImage("Logo.png");
  about = loadImage("about.png");
  help = loadImage("help.png");
  aid = loadImage("aid.png");
  info = loadImage("info.png");

  image(bck, 0, 0, displayWidth, displayHeight);
  //image(logo, displayWidth/6, displayHeight/3);//, displayWidth/2, displayHeight/4);
  //  image(about, 0, 8*displayHeight/10, displayWidth/6, 2*displayHeight/10);
  //  image(help, 5*displayWidth/6, 8*displayHeight/10, displayWidth/6, 2*displayHeight/10);

  cp5.addButton("LOGO")
    .setPosition(displayWidth/7, displayHeight/40)
      .setImages(logo, logo, logo)
        .updateSize();

  cp5.addButton("About")
    .setPosition(10, 9*displayHeight/10)
      .setImages(about, about, about)
        .updateSize();


  cp5.addButton("Help")
    .setPosition(4.1*displayWidth/6, 9*displayHeight/10)
      .setImages(help, help, help)
        .updateSize();
        
  
}

void draw()
{

  if (started == true && mem_inplay == false && ayo_inplay == false && space_inplay == true)
  {
    player.start();
    playa.start();
    player.setLooping(true);
    player.setVolume(1.0, 1.0);
    playa.setLooping(true);
    playa.setVolume(1.0, 1.0);
    sp_draw();
  }
}



void mousePressed()
{

  //  if ( started == false)
  //  {
  //    image(bck, 0, 0, displayWidth, displayHeight);
  //    vibe.vibrate(200);
  //    cp5.getController("LOGO").setVisible(false);
  //    Buttons_dec();
  //    started = true;
  //  }

  if (started == true  && mem_inplay == true && ayo_inplay == false && space_inplay == false)
  {
    Memory.click();
  }

  else if (started == true && mem_inplay == false && space_inplay == false && ayo_inplay == true)
  {
    ayo.clicks();
    ayo.control();
  }
}


void Buttons_dec()
{

  PImage[] ayo = {
    loadImage("Ayo_B.png"), loadImage("Ayo_B.png"), loadImage("Ayo_B.png")
    };
  PImage[] sp = {
    loadImage("SpacePatrol.png"), loadImage("SpacePatrol.png"), loadImage("SpacePatrol.png")
    };
  PImage[] memz = {
    loadImage("Mem_B.png"), loadImage("Mem_B.png"), loadImage("Mem_B.png")
    };

    cp5.addButton("Ayo")
      .setPosition(10, 0.5*displayHeight/10)
        .setImages(ayo[0], ayo[1], ayo[2])
          .updateSize()
            .isVisible();

  cp5.addButton("Space")
    .setPosition(10+0.7*displayWidth/4, 3.8*displayHeight/10)
      .setImages(sp[0], sp[1], sp[2])
        .updateSize();

  cp5.addButton("Memory")
    .setPosition(10+2.2*displayWidth/4, 7*displayHeight/10)
      .setImages(memz[0], memz[1], memz[2])
        .updateSize();
  
  cp5.addButton("Main_Menu")
    .setPosition(displayWidth/3, 9*displayHeight/10)
      .setImages(loadImage("menu.png"), loadImage("menu.png"), loadImage("menu.png"))
        .updateSize();
}

//void back()
//{
//  cpb.addButton("Main_Menu")
//    .setPosition(displayWidth/3, 7*displayHeight/10)
//      .setImages(loadImage("menu.png"), loadImage("menu.png"), loadImage("menu.png"))
//        .updateSize();
//}


public void controlEvent(ControlEvent theEvent) 
{
//  cp5.getController("Ayo").setVisible(false);
//  cp5.getController("Space").setVisible(false);
//  cp5.getController("Memory").setVisible(false);
//  cp5.getController("LOGO").setVisible(false);
//  cp5.getController("About").setVisible(false);
//  cp5.getController("Help").setVisible(false);
//  cpb.getController("Main_Menu").setVisible(false);
  println(theEvent.getController().getName());
}

public void LOGO(int val)
{

  //Buttons_dec();
  image(bck, 0, 0, displayWidth, displayHeight);
  vibe.vibrate(200);
  cp5.getController("LOGO").setVisible(false);
  cp5.getController("About").setVisible(false);
  cp5.getController("Help").setVisible(false);
  Buttons_dec();
  cp5.getController("Main_Menu").setVisible(false);
  started = true;
  //cp5.remove();
}

public void About(int val)
{
  background(125, 10, 250);
 // cp5.getController("LOGO").setVisible(false);
  image(aid, displayWidth/6,displayHeight/4,2*displayWidth/3,3*displayHeight/4);
}

public void Help(int val)
{
  background(255, 0, 200);
  //cp5.getController("LOGO").setVisible(false);
  image(info, displayWidth/6,displayHeight/4,2*displayWidth/3,3*displayHeight/4);
}

public void Ayo(int val)
{
  vibe.vibrate(500);
  cp5.getController("Ayo").setVisible(false);
  cp5.getController("Space").setVisible(false);
  cp5.getController("Memory").setVisible(false);
  cp5.getController("LOGO").setVisible(false);
  cp5.getController("About").setVisible(false);
  cp5.getController("Help").setVisible(false);
  cp5.getController("Main_Menu").setVisible(true);
  //cpb.getController("Main_Menu").setVisible(false);
  ayo = new Ayo();
  ayo.display();
  drum.start();
  drum.setLooping(true);
  ayo_inplay = true;
  mem_inplay = false;
  space_inplay = false;
  Memory= null;
  //ayo = null;

}

public void Space(int val)
{
  vibe.vibrate(500);
  cp5.getController("Ayo").setVisible(false);
  cp5.getController("Space").setVisible(false);
  cp5.getController("Memory").setVisible(false);
  cp5.getController("LOGO").setVisible(false);
  cp5.getController("About").setVisible(false);
  cp5.getController("Help").setVisible(false);
  cp5.getController("Main_Menu").setVisible(true);
  //cpb.getController("Main_Menu").setVisible(false);
  reset();

  space_inplay = true;
  ayo_inplay = false;
  mem_inplay = false;
  Memory= null;
  ayo = null;

}

public void Memory(int val)
{

  vibe.vibrate(500);
  cp5.getController("Ayo").setVisible(false);
  cp5.getController("Space").setVisible(false);
  cp5.getController("Memory").setVisible(false);
  cp5.getController("LOGO").setVisible(false);
  cp5.getController("About").setVisible(false);
  cp5.getController("Help").setVisible(false);
  cp5.getController("Main_Menu").setVisible(true);
  //cpb.getController("Main_Menu").setVisible(false);
  Memory = new Mem(int(displayWidth/5), int(displayHeight/5));
  Memory.display();
  mem_inplay = true; 
  ayo_inplay = false;
  space_inplay = false;
 // Memory= null;
  ayo = null;

}

public void Main_Menu(int val)
{
  vibe.vibrate(500);
  player.pause();
  playa.pause();
  drum.pause();
  image(bck, 0, 0, displayWidth, displayHeight);
  Buttons_dec();
  cp5.getController("Main_Menu").setVisible(false);
  space_inplay = false;
}

