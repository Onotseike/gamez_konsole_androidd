package processing.test.gamez_konsole;

import processing.core.*; 
import processing.data.*; 
import processing.event.*; 
import processing.opengl.*; 

import apwidgets.*; 
import controlP5.*; 
import apwidgets.*; 
import ketai.ui.*; 
import apwidgets.*; 
import apwidgets.*; 

import java.util.HashMap; 
import java.util.ArrayList; 
import java.io.File; 
import java.io.BufferedReader; 
import java.io.PrintWriter; 
import java.io.InputStream; 
import java.io.OutputStream; 
import java.io.IOException; 

public class Gamez_Konsole extends PApplet {







//Objects
Mem Memory;
Ayo ayo;
ArrayList<Misil> misiles;
Nave nave;

KetaiVibrate vibe;
ControlP5 cp5;
APMediaPlayer player, playa,drum;


//variables
boolean started = false;
boolean mem_inplay = false;
boolean space_inplay = false;
boolean ayo_inplay = false;
PImage bck, logo, about, help,aid,info;


float velocidad;
int puntos;
int vidas;
int bonus = 40;
int contBonus = 0;
int atractor;
boolean tokenVida;
boolean tokenAtractor;



public void setup()
{
  //size(displayWidth, displayHeight);
//  orientation(LANDSCAPE);
  vibe = new KetaiVibrate(this);
  cp5 = new ControlP5(this);
  player = new APMediaPlayer(this);
  player.setMediaFile("soundtrack1.mp3");
  playa = new APMediaPlayer(this);
  playa.setMediaFile("soundtrack2.mp3");
  drum = new APMediaPlayer(this);
  drum.setMediaFile("drum.mp3");

  Memory = new Mem(PApplet.parseInt(displayWidth/5), PApplet.parseInt(displayHeight/5)); 
  


  bck = loadImage("Bck.png");
  logo = loadImage("Logo.png");
  about = loadImage("about.png");
  help = loadImage("help.png");
  aid = loadImage("aid.png");
  info = loadImage("info.png");

  image(bck, 0, 0, displayWidth, displayHeight);
  //image(logo, displayWidth/6, displayHeight/3);//, displayWidth/2, displayHeight/4);
  //  image(about, 0, 8*displayHeight/10, displayWidth/6, 2*displayHeight/10);
  //  image(help, 5*displayWidth/6, 8*displayHeight/10, displayWidth/6, 2*displayHeight/10);

  cp5.addButton("LOGO")
    .setPosition(displayWidth/7, displayHeight/40)
      .setImages(logo, logo, logo)
        .updateSize();

  cp5.addButton("About")
    .setPosition(10, 9*displayHeight/10)
      .setImages(about, about, about)
        .updateSize();


  cp5.addButton("Help")
    .setPosition(4.1f*displayWidth/6, 9*displayHeight/10)
      .setImages(help, help, help)
        .updateSize();
        
  
}

public void draw()
{

  if (started == true && mem_inplay == false && ayo_inplay == false && space_inplay == true)
  {
    player.start();
    playa.start();
    player.setLooping(true);
    player.setVolume(1.0f, 1.0f);
    playa.setLooping(true);
    playa.setVolume(1.0f, 1.0f);
    sp_draw();
  }
}



public void mousePressed()
{

  //  if ( started == false)
  //  {
  //    image(bck, 0, 0, displayWidth, displayHeight);
  //    vibe.vibrate(200);
  //    cp5.getController("LOGO").setVisible(false);
  //    Buttons_dec();
  //    started = true;
  //  }

  if (started == true  && mem_inplay == true && ayo_inplay == false && space_inplay == false)
  {
    Memory.click();
  }

  else if (started == true && mem_inplay == false && space_inplay == false && ayo_inplay == true)
  {
    ayo.clicks();
    ayo.control();
  }
}


public void Buttons_dec()
{

  PImage[] ayo = {
    loadImage("Ayo_B.png"), loadImage("Ayo_B.png"), loadImage("Ayo_B.png")
    };
  PImage[] sp = {
    loadImage("SpacePatrol.png"), loadImage("SpacePatrol.png"), loadImage("SpacePatrol.png")
    };
  PImage[] memz = {
    loadImage("Mem_B.png"), loadImage("Mem_B.png"), loadImage("Mem_B.png")
    };

    cp5.addButton("Ayo")
      .setPosition(10, 0.5f*displayHeight/10)
        .setImages(ayo[0], ayo[1], ayo[2])
          .updateSize()
            .isVisible();

  cp5.addButton("Space")
    .setPosition(10+0.7f*displayWidth/4, 3.8f*displayHeight/10)
      .setImages(sp[0], sp[1], sp[2])
        .updateSize();

  cp5.addButton("Memory")
    .setPosition(10+2.2f*displayWidth/4, 7*displayHeight/10)
      .setImages(memz[0], memz[1], memz[2])
        .updateSize();
  
  cp5.addButton("Main_Menu")
    .setPosition(displayWidth/3, 9*displayHeight/10)
      .setImages(loadImage("menu.png"), loadImage("menu.png"), loadImage("menu.png"))
        .updateSize();
}

//void back()
//{
//  cpb.addButton("Main_Menu")
//    .setPosition(displayWidth/3, 7*displayHeight/10)
//      .setImages(loadImage("menu.png"), loadImage("menu.png"), loadImage("menu.png"))
//        .updateSize();
//}


public void controlEvent(ControlEvent theEvent) 
{
//  cp5.getController("Ayo").setVisible(false);
//  cp5.getController("Space").setVisible(false);
//  cp5.getController("Memory").setVisible(false);
//  cp5.getController("LOGO").setVisible(false);
//  cp5.getController("About").setVisible(false);
//  cp5.getController("Help").setVisible(false);
//  cpb.getController("Main_Menu").setVisible(false);
  println(theEvent.getController().getName());
}

public void LOGO(int val)
{

  //Buttons_dec();
  image(bck, 0, 0, displayWidth, displayHeight);
  vibe.vibrate(200);
  cp5.getController("LOGO").setVisible(false);
  cp5.getController("About").setVisible(false);
  cp5.getController("Help").setVisible(false);
  Buttons_dec();
  cp5.getController("Main_Menu").setVisible(false);
  started = true;
  //cp5.remove();
}

public void About(int val)
{
  background(125, 10, 250);
 // cp5.getController("LOGO").setVisible(false);
  image(aid, displayWidth/6,displayHeight/4,2*displayWidth/3,3*displayHeight/4);
}

public void Help(int val)
{
  background(255, 0, 200);
  //cp5.getController("LOGO").setVisible(false);
  image(info, displayWidth/6,displayHeight/4,2*displayWidth/3,3*displayHeight/4);
}

public void Ayo(int val)
{
  vibe.vibrate(500);
  cp5.getController("Ayo").setVisible(false);
  cp5.getController("Space").setVisible(false);
  cp5.getController("Memory").setVisible(false);
  cp5.getController("LOGO").setVisible(false);
  cp5.getController("About").setVisible(false);
  cp5.getController("Help").setVisible(false);
  cp5.getController("Main_Menu").setVisible(true);
  //cpb.getController("Main_Menu").setVisible(false);
  ayo = new Ayo();
  ayo.display();
  drum.start();
  drum.setLooping(true);
  ayo_inplay = true;
  mem_inplay = false;
  space_inplay = false;
  Memory= null;
  //ayo = null;

}

public void Space(int val)
{
  vibe.vibrate(500);
  cp5.getController("Ayo").setVisible(false);
  cp5.getController("Space").setVisible(false);
  cp5.getController("Memory").setVisible(false);
  cp5.getController("LOGO").setVisible(false);
  cp5.getController("About").setVisible(false);
  cp5.getController("Help").setVisible(false);
  cp5.getController("Main_Menu").setVisible(true);
  //cpb.getController("Main_Menu").setVisible(false);
  reset();

  space_inplay = true;
  ayo_inplay = false;
  mem_inplay = false;
  Memory= null;
  ayo = null;

}

public void Memory(int val)
{

  vibe.vibrate(500);
  cp5.getController("Ayo").setVisible(false);
  cp5.getController("Space").setVisible(false);
  cp5.getController("Memory").setVisible(false);
  cp5.getController("LOGO").setVisible(false);
  cp5.getController("About").setVisible(false);
  cp5.getController("Help").setVisible(false);
  cp5.getController("Main_Menu").setVisible(true);
  //cpb.getController("Main_Menu").setVisible(false);
  Memory = new Mem(PApplet.parseInt(displayWidth/5), PApplet.parseInt(displayHeight/5));
  Memory.display();
  mem_inplay = true; 
  ayo_inplay = false;
  space_inplay = false;
 // Memory= null;
  ayo = null;

}

public void Main_Menu(int val)
{
  vibe.vibrate(500);
  player.pause();
  playa.pause();
  drum.pause();
  image(bck, 0, 0, displayWidth, displayHeight);
  Buttons_dec();
  cp5.getController("Main_Menu").setVisible(false);
  space_inplay = false;
}

public class Ayo
{
  //Fields
  PImage[] ayo_img = new PImage[4];
  PFont font;
  int P1, P2, c1, c2, click1, click2, click3, click4, click5, click6, click7, click8, click9, click10, click11, click12;
  IntList hole;
  boolean started = true;
  boolean inplay = false;
  float player = random(0, 1);
  String turn1 = "PLAYER 1";
  String turn2 = "PLAYER 2";
  String turn = "";


  // Methods

  // constructor
  Ayo()
  {
    font = loadFont("Bauhaus93-48.vlw");
    ayo_img[0] = loadImage("Ayoo.jpg");
    ayo_img[1]=loadImage("Ayo_Board.png");
    ayo_img[2] = loadImage("Ayo_seed.png");
    ayo_img[3] = loadImage("plain_board.png");

    P1 = 0; 
    P2= 0; 
    c1 =0; 
    c2 = 0;
    click1 = 0;
    click2 = 0;
    click3 = 0;
    click4 = 0;
    click5 = 0;
    click6 = 0;
    click7 = 0;
    click8 = 0;
    click9 = 0;
    click10 = 0;
    click11 = 0;
    click12 = 0;
    hole = new IntList();

    for (int i = 0 ; i < 12 ; i++)
    {
      hole.append(1);
    }
    println(hole);
  }

  // Properties function
  public void display()
  {

    image(ayo_img[0], 0, 0, displayWidth, displayHeight);
    image(ayo_img[1], displayWidth/7, displayHeight/4, 5*(displayWidth/7), 2*(displayHeight/4));
    image(ayo_img[3], 0, 3* (displayHeight/8), displayWidth/7, displayHeight/4);
    image(ayo_img[3], 6*(displayWidth/7), 3*(displayHeight/8), displayWidth/7, displayHeight/4);

    pushStyle();
    noStroke();
    fill(205, 133, 63);    
    //hints
    rect(displayWidth/3, 0, 2*(displayWidth/3), displayHeight/10);
    rect(0, 9*(displayHeight/10), 2*(displayWidth/3), displayHeight/10);
    // Players
    rect(0, 0, displayWidth/5, displayHeight/10);
    rect(4*(displayWidth/5), 9*(displayHeight/10), displayWidth/5, displayHeight/10);
    // Score
    rect(displayWidth/28, displayHeight/4, displayWidth/14, displayHeight/8);
    rect(25.5f*(displayWidth/28), displayHeight/4, (displayWidth/14), displayHeight/8);

    for ( int r =0; r < 6; r++)
    {
      rect(displayWidth/5.5f+ (r* displayWidth/8.6f), displayHeight/8, displayWidth/18, displayHeight/8);
      rect(displayWidth/5.5f+ (r* displayWidth/8.6f), 3*(displayHeight/4), displayWidth/18, displayHeight/8);
    }

    popStyle();

    pushStyle();
    textFont(font);
    textSize(40);
    fill(0);
    text("PLAYER 1", 4, 30);
    text("PLAYER 2", 4+ 4*(displayWidth/5), 30+ 9*(displayHeight/10));
    pushStyle();

    if ( player >= 0 && player < 0.5f)
    {
      turn = turn1;
      pushStyle();
      fill(0);
      textFont(font);
      textSize(20);
      text( turn + " Start", 2*displayWidth/3, 30);
      popStyle();
    }
    else if ( player > 0.5f && player <= 1)
    {
      turn = turn2;
      pushStyle();
      fill(0);
      textSize(20);
      text(turn + "  Start", displayWidth/6, 30+9*displayHeight/10);
      popStyle();
    }


    if (started)
    {

      // for hole a  
      image(ayo_img[2], 5.0f*displayWidth/28, 2.6f*displayHeight/8, 20, 20);
      image(ayo_img[2], 5.0f* displayWidth/28, 3.3f*displayHeight/8, 20, 20);
      image(ayo_img[2], 6.3f*displayWidth/28, 2.6f*displayHeight/8, 20, 20);
      image(ayo_img[2], 6.3f*displayWidth/28, 3.3f*displayHeight/8, 20, 20);
      hole.set(0, 4);
      pushStyle();
      textSize(30);
      fill(0);
      text(hole.get(0), 10+ displayWidth/5.5f + 0*displayWidth/8.6f, 40 + displayHeight/8);
      popStyle();


      // for hole b
      image(ayo_img[2], 5.0f*displayWidth/28, 4.5f*displayHeight/8, 20, 20);
      image(ayo_img[2], 5.0f*displayWidth/28, 5.2f*displayHeight/8, 20, 20);
      image(ayo_img[2], 6.3f*displayWidth/28, 4.5f*displayHeight/8, 20, 20);
      image(ayo_img[2], 6.3f*displayWidth/28, 5.2f*displayHeight/8, 20, 20);
      hole.set(1, 4);
      pushStyle();
      textSize(30);
      fill(0);
      text(hole.get(1), 10 + displayWidth/5.5f + 0*displayWidth/8.6f, 40 + 3*displayHeight/4);
      popStyle();

      // for hole c
      image(ayo_img[2], 8.1f*displayWidth/28, 2.6f*displayHeight/8, 20, 20);
      image(ayo_img[2], 8.1f*displayWidth/28, 3.3f*displayHeight/8, 20, 20);
      image(ayo_img[2], 9.3f*displayWidth/28, 2.6f*displayHeight/8, 20, 20);
      image(ayo_img[2], 9.3f*displayWidth/28, 3.3f*displayHeight/8, 20, 20);
      hole.set(2, 4);
      pushStyle();
      textSize(30);
      fill(0);
      text(hole.get(2), 10 + displayWidth/5.5f + 1*displayWidth/8.6f, 40 + displayHeight/8);
      popStyle();

      // for hole d
      image(ayo_img[2], 8.1f*displayWidth/28, 4.5f*displayHeight/8, 20, 20);
      image(ayo_img[2], 8.1f*displayWidth/28, 5.2f*displayHeight/8, 20, 20);
      image(ayo_img[2], 9.3f*displayWidth/28, 4.5f*displayHeight/8, 20, 20);
      image(ayo_img[2], 9.3f*displayWidth/28, 5.2f*displayHeight/8, 20, 20);
      hole.set(3, 4);
      pushStyle();
      textSize(30);
      fill(0);
      text(hole.get(3), 10 + displayWidth/5.5f + 1*displayWidth/8.6f, 40 + 3*displayHeight/4);
      popStyle();

      //for hole e
      image(ayo_img[2], 11.4f*displayWidth/28, 2.6f*displayHeight/8, 20, 20);
      image(ayo_img[2], 11.4f*displayWidth/28, 3.3f*displayHeight/8, 20, 20);
      image(ayo_img[2], 12.6f*displayWidth/28, 2.6f*displayHeight/8, 20, 20);
      image(ayo_img[2], 12.6f*displayWidth/28, 3.3f*displayHeight/8, 20, 20);
      hole.set(4, 4);
      pushStyle();
      textSize(30);
      fill(0);
      text(hole.get(4), 10 + displayWidth/5.5f + 2*displayWidth/8.6f, 40 + displayHeight/8);
      popStyle();

      // for hole f
      image(ayo_img[2], 11.4f*displayWidth/28, 4.5f*displayHeight/8, 20, 20);
      image(ayo_img[2], 11.4f*displayWidth/28, 5.2f*displayHeight/8, 20, 20);
      image(ayo_img[2], 12.6f*displayWidth/28, 4.5f*displayHeight/8, 20, 20);
      image(ayo_img[2], 12.6f*displayWidth/28, 5.2f*displayHeight/8, 20, 20);
      hole.set(5, 4);
      pushStyle();
      textSize(30);
      fill(0);
      text(hole.get(5), 10 + displayWidth/5.5f + 2*displayWidth/8.6f, 40 + 3*displayHeight/4);
      popStyle();

      // for hole g
      image(ayo_img[2], 14.8f*displayWidth/28, 2.6f*displayHeight/8, 20, 20);
      image(ayo_img[2], 14.8f*displayWidth/28, 3.3f*displayHeight/8, 20, 20);
      image(ayo_img[2], 16*displayWidth/28, 2.6f*displayHeight/8, 20, 20);
      image(ayo_img[2], 16*displayWidth/28, 3.3f*displayHeight/8, 20, 20);
      hole.set(6, 4);
      pushStyle();
      textSize(30);
      fill(0);
      text(hole.get(6), 10 + displayWidth/5.5f + 3*displayWidth/8.6f, 40 + displayHeight/8);
      popStyle();

      // for hole h
      image(ayo_img[2], 14.8f*displayWidth/28, 4.5f*displayHeight/8, 20, 20);
      image(ayo_img[2], 14.8f*displayWidth/28, 5.2f*displayHeight/8, 20, 20);
      image(ayo_img[2], 16*displayWidth/28, 4.5f*displayHeight/8, 20, 20);
      image(ayo_img[2], 16*displayWidth/28, 5.2f*displayHeight/8, 20, 20);
      hole.set(7, 4);
      pushStyle();
      textSize(30);
      fill(0);
      text(hole.get(7), 10 + displayWidth/5.5f + 3*displayWidth/8.6f, 40 + 3*displayHeight/4);
      popStyle();

      // for hole i
      image(ayo_img[2], 18*displayWidth/28, 2.6f*displayHeight/8, 20, 20);
      image(ayo_img[2], 18*displayWidth/28, 3.3f*displayHeight/8, 20, 20);
      image(ayo_img[2], 19.2f*displayWidth/28, 2.6f*displayHeight/8, 20, 20);
      image(ayo_img[2], 19.2f*displayWidth/28, 3.3f*displayHeight/8, 20, 20);
      hole.set(8, 4);
      pushStyle();
      textSize(30);
      fill(0);
      text(hole.get(8), 10 + displayWidth/5.5f + 4*displayWidth/8.6f, 40 + displayHeight/8);
      popStyle();

      // for hole j
      image(ayo_img[2], 18*displayWidth/28, 4.5f*displayHeight/8, 20, 20);
      image(ayo_img[2], 18*displayWidth/28, 5.2f*displayHeight/8, 20, 20);
      image(ayo_img[2], 19.2f*displayWidth/28, 4.5f*displayHeight/8, 20, 20);
      image(ayo_img[2], 19.2f*displayWidth/28, 5.2f*displayHeight/8, 20, 20);
      hole.set(9, 4);
      pushStyle();
      textSize(30);
      fill(0);
      text(hole.get(9), 10 + displayWidth/5.5f + 4*displayWidth/8.6f, 40 + 3*displayHeight/4);
      popStyle();

      // for hole k
      image(ayo_img[2], 21*displayWidth/28, 2.6f*displayHeight/8, 20, 20);
      image(ayo_img[2], 21*displayWidth/28, 3.3f*displayHeight/8, 20, 20);
      image(ayo_img[2], 22.2f*displayWidth/28, 2.6f*displayHeight/8, 20, 20);
      image(ayo_img[2], 22.2f*displayWidth/28, 3.3f*displayHeight/8, 20, 20);
      hole.set(10, 4);
      pushStyle();
      textSize(30);
      fill(0);
      text(hole.get(10), 10 + displayWidth/5.5f + 5*displayWidth/8.6f, 40 + displayHeight/8);
      popStyle();

      // for hole l
      image(ayo_img[2], 21*displayWidth/28, 4.5f*displayHeight/8, 20, 20);
      image(ayo_img[2], 21*displayWidth/28, 5.2f*displayHeight/8, 20, 20);
      image(ayo_img[2], 22.2f*displayWidth/28, 4.5f*displayHeight/8, 20, 20);
      image(ayo_img[2], 22.2f*displayWidth/28, 5.2f*displayHeight/8, 20, 20);
      hole.set(11, 4);
      pushStyle();
      textSize(30);
      fill(0);
      text(hole.get(11), 10 + displayWidth/5.5f + 5*displayWidth/8.6f, 40 + 3*displayHeight/4);
      popStyle();

      started = false;
    }
  }
  public void clicks()
  {
    if (mousePressed == true)
    {
      if ( mouseX > 95.9f* displayWidth/588 && mouseX < 151.9f* displayWidth/588 && mouseY > 5.2f* displayHeight/16 && mouseY < 7.2f* displayHeight/16)
      {
        float ax = constrain(mouseX, 95.9f* displayWidth/588, 151.9f* displayWidth/588);
        float ay = constrain(mouseY, 5.2f* displayHeight/16, 7.2f* displayHeight/16);
        image(ayo_img[2], ax, ay, 20, 20);
      }

      if ( mouseX > 95.9f* displayWidth/588 && mouseX < 151.9f* displayWidth/588 && mouseY > 9* displayHeight/16 && mouseY < 11* displayHeight/16)
      {
        float bx = constrain(mouseX, 95.9f* displayWidth/588, 151.9f* displayWidth/588);
        float by = constrain(mouseY, 9* displayHeight/16, 11* displayHeight/16);
        image(ayo_img[2], bx, by, 20, 20);
      }
      if ( mouseX > 161* displayWidth/588 && mouseX < 217* displayWidth/588 && mouseY > 5.2f* displayHeight/16 && mouseY < 7.2f* displayHeight/16)
      {
        float cx = constrain(mouseX, 161* displayWidth/588, 217* displayWidth/588);
        float cy = constrain(mouseY, 5.2f* displayHeight/16, 7.2f* displayHeight/16);
        image(ayo_img[2], cx, cy, 20, 20);
      }
      if ( mouseX > 161* displayWidth/588 && mouseX < 217* displayWidth/588 && mouseY > 9* displayHeight/16 && mouseY < 11* displayHeight/16)
      {
        float dx = constrain(mouseX, 161* displayWidth/588, 217* displayWidth/588);
        float dy = constrain(mouseY, 9* displayHeight/16, 11* displayHeight/16);
        image(ayo_img[2], dx, dy, 20, 20);
      }
      if ( mouseX > 230.3f* displayWidth/588 && mouseX < 286.3f* displayWidth/588 && mouseY > 5.2f* displayHeight/16 && mouseY < 7.2f* displayHeight/16)
      {
        float ex = constrain(mouseX, 230.3f* displayWidth/588, 286.3f* displayWidth/588);
        float ey = constrain(mouseY, 5.2f* displayHeight/16, 7.2f* displayHeight/16);
        image(ayo_img[2], ex, ey, 20, 20);
      }
      if ( mouseX > 230.3f* displayWidth/588 && mouseX < 286.3f* displayWidth/588 && mouseY > 9* displayHeight/16 && mouseY < 11* displayHeight/16)
      {
        float fx = constrain(mouseX, 230.3f* displayWidth/588, 286.3f* displayWidth/588);
        float fy = constrain(mouseY, 9* displayHeight/16, 11* displayHeight/16);
        image(ayo_img[2], fx, fy, 20, 20);
      }
      if ( mouseX > 305.9f* displayWidth/588 && mouseX < 361.9f* displayWidth/588 && mouseY > 5.2f* displayHeight/16 && mouseY < 7.2f* displayHeight/16)
      {
        float gx = constrain(mouseX, 305.9f* displayWidth/588, 361.9f* displayWidth/588);
        float gy = constrain(mouseY, 5.2f* displayHeight/16, 7.2f* displayHeight/16);
        image(ayo_img[2], gx, gy, 20, 20);
      }
      if ( mouseX > 305.9f* displayWidth/588 && mouseX < 361.9f* displayWidth/588 && mouseY > 9* displayHeight/16 && mouseY < 11* displayHeight/16)
      {
        float hx = constrain(mouseX, 305.9f* displayWidth/588, 361.9f* displayWidth/588);
        float hy = constrain(mouseY, 9* displayHeight/16, 11* displayHeight/16);
        image(ayo_img[2], hx, hy, 20, 20);
      }
      if ( mouseX > 368.9f* displayWidth/588 && mouseX < 424.9f* displayWidth/588 && mouseY > 5.2f* displayHeight/16 && mouseY < 7.2f* displayHeight/16)
      {
        float ix = constrain(mouseX, 368.9f* displayWidth/588, 424.9f* displayWidth/588);
        float iy = constrain(mouseY, 5.2f* displayHeight/16, 7.2f* displayHeight/16);
        image(ayo_img[2], ix, iy, 20, 20);
      }
      if ( mouseX > 368.9f* displayWidth/588 && mouseX < 424.9f* displayWidth/588 && mouseY > 9* displayHeight/16 && mouseY < 11* displayHeight/16)
      {
        float jx = constrain(mouseX, 368.9f* displayWidth/588, 424.9f* displayWidth/588);
        float jy = constrain(mouseY, 9* displayHeight/16, 11* displayHeight/16);
        image(ayo_img[2], jx, jy, 20, 20);
      }
      if ( mouseX > 31* displayWidth/42 && mouseX < 5* displayWidth/6 && mouseY > 5.2f* displayHeight/16 && mouseY < 7.2f* displayHeight/16)
      {
        float kx = constrain(mouseX, 31* displayWidth/42, 5* displayWidth/6);
        float ky = constrain(mouseY, 5.2f* displayHeight/16, 7.2f* displayHeight/16);
        image(ayo_img[2], kx, ky, 20, 20);
      }
      if ( mouseX > 31* displayWidth/42 && mouseX < 5* displayWidth/6  && mouseY > 9* displayHeight/16 && mouseY < 11* displayHeight/16)
      {
        float lx = constrain(mouseX, 31* displayWidth/42, 5* displayWidth/6);
        float ly = constrain(mouseY, 9* displayHeight/16, 11* displayHeight/16);
        image(ayo_img[2], lx, ly, 20, 20);
      }
    }

    if ( mouseX > 95.9f* displayWidth/588 && mouseX < 151.9f* displayWidth/588 && mouseY > 5.2f* displayHeight/16 && mouseY < 7.2f* displayHeight/16)
    {
      if (click1 ==1)
      {
        fill(205, 133, 63);
        rect(displayWidth/5.5f+ (0* displayWidth/8.6f), displayHeight/8, displayWidth/18, displayHeight/8);
        hole.set(0, hole.get(0)+1);
        c1 += 1;
        println("Counter 1 = " + c1);
        println("a = " + hole.get(0) );
        pushStyle();
        textSize(30);
        fill(0);
        text(hole.get(0), 10+ displayWidth/5.5f + 0*displayWidth/8.6f, 40 + displayHeight/8);
        popStyle();
        // lower rect for hints
        if (hole.get(0) == 1 && c1 == c2)
        {
          if (turn == "PLAYER 1")
          {
            fill(205, 133, 63);
            rect(displayWidth/3, 0, 2*(displayWidth/3), displayHeight/10);
            //rect(0, 9*(displayHeight/10), 2*(displayWidth/3), displayHeight/10);
            fill(0);
            textSize(30);
            text("It's " + turn2 + " Turn. ", 15 + displayWidth/3, 30);
            turn = turn2;
          }
          else if ( turn == "PLAYER 2")
          {
            fill(205, 133, 63);
            rect(0, 9*(displayHeight/10), 2*(displayWidth/3), displayHeight/10);
            fill(0);
            textSize(30);
            text("It's " + turn1 + "Turn. ", 15, 30 + 9*displayHeight/10);
          }

          // rect(displayWidth/5.5+ (r* displayWidth/8.6), 3*(displayHeight/4), displayWidth/18, displayHeight/8);
        }
        click1 -= 1;
      }
      else if (click1 == 0)
      {
        controla();
        click1 +=1;
        //click1 = 1;
        click2 = 1;
        click3 = 1;
        click4 = 1;
        click5 = 1;
        click6 = 1;
        click7 = 1;
        click8 = 1;
        click9 = 1;
        click10 = 1;
        click11 = 1;
        click12 = 1;
      }
    }


    if ( mouseX > 95.9f* displayWidth/588 && mouseX < 151.9f* displayWidth/588 && mouseY > 9* displayHeight/16 && mouseY < 11* displayHeight/16)
    {
      if ( click2 == 1)
      {
        fill(205, 133, 63);
        rect(displayWidth/5.5f+ (0* displayWidth/8.6f), 3*(displayHeight/4), displayWidth/18, displayHeight/8);
        //rect(displayWidth/5.5+ (0* displayWidth/8.6), displayHeight/8, displayWidth/18, displayHeight/8);
        hole.set(1, hole.get(1)+1);
        c1 += 1;
        println("Counter 1 = " + c1);
        println("a = " + hole.get(1) );
        pushStyle();
        textSize(30);
        fill(0);
        text(hole.get(1), 10+ displayWidth/5.5f + 0*displayWidth/8.6f, 40 + 6*displayHeight/8);
        popStyle();
        // lower rect for hints
        if (hole.get(1) == 1 && c1 == c2)
        {
          if (turn == "PLAYER 1")
          {
            fill(205, 133, 63);
            rect(displayWidth/3, 0, 2*(displayWidth/3), displayHeight/10);
            //rect(0, 9*(displayHeight/10), 2*(displayWidth/3), displayHeight/10);
            fill(0);
            textSize(30);
            text("It's " + turn2 + " Turn. ", 15 + displayWidth/3, 30);
            turn = turn2;
          }
          else if ( turn == "PLAYER 2")
          {
            fill(205, 133, 63);
            rect(0, 9*(displayHeight/10), 2*(displayWidth/3), displayHeight/10);
            fill(0);
            textSize(30);
            text("It's " + turn1 + "Turn. ", 15, 30 + 9*displayHeight/10);
          }
        }
        click2 -= 1;
      }
      else if (click2 == 0) 
      {
        controlb();
        click2 += 1;
        click1 = 1;
        //click2 = 1;
        click3 = 1;
        click4 = 1;
        click5 = 1;
        click6 = 1;
        click7 = 1;
        click8 = 1;
        click9 = 1;
        click10 = 1;
        click11 = 1;
        click12 = 1;
      }
    }


    if ( mouseX > 161* displayWidth/588 && mouseX < 217* displayWidth/588 && mouseY > 5.2f* displayHeight/16 && mouseY < 7.2f* displayHeight/16)
    {

      if (click3 == 1)
      {

        fill(205, 133, 63);
        rect(displayWidth/5.5f+ (1* displayWidth/8.6f), displayHeight/8, displayWidth/18, displayHeight/8);
        hole.set(2, hole.get(2)+1);
        c1 += 1;
        println("Counter 1 = " + c1);
        println("a = " + hole.get(2)); 

        pushStyle();
        textSize(30);
        fill(0);
        text(hole.get(2), 10+ displayWidth/5.5f + 1*displayWidth/8.6f, 40 + displayHeight/8);
        popStyle();
        // lower rect for hints
        if (hole.get(2) == 1 && c1 == c2)
        {
          if (turn == "PLAYER 1")
          {
            fill(205, 133, 63);
            rect(displayWidth/3, 0, 2*(displayWidth/3), displayHeight/10);
            //rect(0, 9*(displayHeight/10), 2*(displayWidth/3), displayHeight/10);
            fill(0);
            textSize(30);
            text("It's " + turn2 + " Turn. ", 15 + displayWidth/3, 30);
            turn = turn2;
          }
          else if ( turn == "PLAYER 2")
          {
            fill(205, 133, 63);
            rect(0, 9*(displayHeight/10), 2*(displayWidth/3), displayHeight/10);
            fill(0);
            textSize(30);
            text("It's " + turn1 + "Turn. ", 15, 30 + 9*displayHeight/10);
          }
        }
        click3 -= 1;
      }
      else if (click3 == 0)
      {
        controlc();
        click3 += 1;
        click1 = 1;
        click2 = 1;
        //click3 = 1;
        click4 = 1;
        click5 = 1;
        click6 = 1;
        click7 = 1;
        click8 = 1;
        click9 = 1;
        click10 = 1;
        click11 = 1;
        click12 = 1;
      }
    }


    if ( mouseX > 161* displayWidth/588 && mouseX < 217* displayWidth/588 && mouseY > 9* displayHeight/16 && mouseY < 11* displayHeight/16)
    {
      if (click4 == 1)
      {


        fill(205, 133, 63);
        rect(displayWidth/5.5f+ (1* displayWidth/8.6f), 3*(displayHeight/4), displayWidth/18, displayHeight/8);
        //rect(displayWidth/5.5+ (0* displayWidth/8.6), displayHeight/8, displayWidth/18, displayHeight/8);
        hole.set(3, hole.get(3)+1);
        c1 += 1;
        println("Counter 1 = " + c1);
        println("a = " + hole.get(3) );
        pushStyle();
        textSize(30);
        fill(0);
        text(hole.get(3), 10+ displayWidth/5.5f + 1*displayWidth/8.6f, 40 + 6*displayHeight/8);
        popStyle();
        // lower rect for hints
        if (hole.get(3) == 1 && c1 == c2)
        {

          if (turn == "PLAYER 1")
          {
            fill(205, 133, 63);
            rect(displayWidth/3, 0, 2*(displayWidth/3), displayHeight/10);
            //rect(0, 9*(displayHeight/10), 2*(displayWidth/3), displayHeight/10);
            fill(0);
            textSize(30);
            text("It's " + turn2 + " Turn. ", 15 + displayWidth/3, 30);
            turn = turn2;
          }
          else if ( turn == "PLAYER 2")
          {
            fill(205, 133, 63);
            rect(0, 9*(displayHeight/10), 2*(displayWidth/3), displayHeight/10);
            fill(0);
            textSize(30);
            text("It's " + turn1 + "Turn. ", 15, 30 + 9*displayHeight/10);
          }
        }
        click4 -= 1;
      }
      else if (click4 == 0)
      {
        controld();
        click4 += 1;
        click1 = 1;
        click2 = 1;
        click3 = 1;
        //click4 = 1;
        click5 = 1;
        click6 = 1;
        click7 = 1;
        click8 = 1;
        click9 = 1;
        click10 = 1;
        click11 = 1;
        click12 = 1;
      }
    }


    if ( mouseX > 230.3f* displayWidth/588 && mouseX < 286.3f* displayWidth/588 && mouseY > 5.2f* displayHeight/16 && mouseY < 7.2f* displayHeight/16)
    {
      if (click5 == 1)
      {

        fill(205, 133, 63);
        rect(displayWidth/5.5f+ (2* displayWidth/8.6f), displayHeight/8, displayWidth/18, displayHeight/8);
        hole.set(4, hole.get(4)+1);
        c1 += 1;
        println("Counter 1 = " + c1);
        println("a = " + hole.get(4));
        pushStyle();
        textSize(30);
        fill(0);
        text(hole.get(4), 10+ displayWidth/5.5f + 2*displayWidth/8.6f, 40 + displayHeight/8);
        popStyle();
        // lower rect for hints
        if (hole.get(4) == 1 && c1 == c2)
        {
          if (turn == "PLAYER 1")
          {
            fill(205, 133, 63);
            rect(displayWidth/3, 0, 2*(displayWidth/3), displayHeight/10);
            //rect(0, 9*(displayHeight/10), 2*(displayWidth/3), displayHeight/10);
            fill(0);
            textSize(30);
            text("It's " + turn2 + " Turn. ", 15 + displayWidth/3, 30);
            turn = turn2;
          }
          else if ( turn == "PLAYER 2")
          {
            fill(205, 133, 63);
            rect(0, 9*(displayHeight/10), 2*(displayWidth/3), displayHeight/10);
            fill(0);
            textSize(30);
            text("It's " + turn1 + "Turn. ", 15, 30 + 9*displayHeight/10);
          }
        }
        click5 -= 1;
      }
      else if (click5 == 0)
      { 
        controle();
        click5 += 1;
        click1 = 1;
        click2 = 1;
        click3 = 1;
        click4 = 1;
        // click5 = 1;
        click6 = 1;
        click7 = 1;
        click8 = 1;
        click9 = 1;
        click10 = 1;
        click11 = 1;
        click12 = 1;
      }
    }


    if ( mouseX > 230.3f* displayWidth/588 && mouseX < 286.3f* displayWidth/588 && mouseY > 9* displayHeight/16 && mouseY < 11* displayHeight/16)
    {
      if (click6 == 1)
      {

        fill(205, 133, 63);
        rect(displayWidth/5.5f+ (2* displayWidth/8.6f), 3*(displayHeight/4), displayWidth/18, displayHeight/8);
        //rect(displayWidth/5.5+ (0* displayWidth/8.6), displayHeight/8, displayWidth/18, displayHeight/8);
        hole.set(5, hole.get(5)+1);
        c1 += 1;
        println("Counter 1 = " + c1);
        println("a = " + hole.get(5) );
        pushStyle();
        textSize(30);
        fill(0);
        text(hole.get(5), 10+ displayWidth/5.5f + 2*displayWidth/8.6f, 40 + 6*displayHeight/8);
        popStyle();
        // lower rect for hints
        if (hole.get(5) == 1 && c1 == c2)
        {

          if (turn == "PLAYER 1")
          {
            fill(205, 133, 63);
            rect(displayWidth/3, 0, 2*(displayWidth/3), displayHeight/10);
            fill(0);
            textSize(30);
            text("It's " + turn2 + " Turn. ", 15 + displayWidth/3, 30);
            turn = turn2;
          }
          else if ( turn == "PLAYER 2")
          {
            fill(205, 133, 63);
            rect(0, 9*(displayHeight/10), 2*(displayWidth/3), displayHeight/10);
            fill(0);
            textSize(30);
            text("It's " + turn1 + "Turn. ", 15, 30 + 9*displayHeight/10);
          }
        }
        click6 -= 1;
      }
      else if (click6 == 0)
      {
        controlf();
        click6 += 1;
        click1 = 1;
        click2 = 1;
        click3 = 1;
        click4 = 1;
        click5 = 1;
        //click6 = 1;
        click7 = 1;
        click8 = 1;
        click9 = 1;
        click10 = 1;
        click11 = 1;
        click12 = 1;
      }
    }


    if ( mouseX > 305.9f* displayWidth/588 && mouseX < 361.9f* displayWidth/588 && mouseY > 5.2f* displayHeight/16 && mouseY < 7.2f* displayHeight/16)
    {
      if (click7 == 1)
      {
        fill(205, 133, 63);
        rect(displayWidth/5.5f+ (3* displayWidth/8.6f), displayHeight/8, displayWidth/18, displayHeight/8);
        hole.set(6, hole.get(6)+1);
        c1 += 1;
        println("Counter 1 = " + c1);
        println("a = " + hole.get(6) );
        pushStyle();
        textSize(30);
        fill(0);
        text(hole.get(6), 10+ displayWidth/5.5f + 3*displayWidth/8.6f, 40 + displayHeight/8);
        popStyle();
        // lower rect for hints
        if (hole.get(6) == 1 && c1 == c2)
        {
          if (turn == "PLAYER 1")
          {
            fill(205, 133, 63);
            rect(displayWidth/3, 0, 2*(displayWidth/3), displayHeight/10);
            //rect(0, 9*(displayHeight/10), 2*(displayWidth/3), displayHeight/10);
            fill(0);
            textSize(30);
            text("It's " + turn2 + " Turn. ", 15 + displayWidth/3, 30);
            turn = turn2;
          }
          else if ( turn == "PLAYER 2")
          {
            fill(205, 133, 63);
            rect(0, 9*(displayHeight/10), 2*(displayWidth/3), displayHeight/10);
            fill(0);
            textSize(30);
            text("It's " + turn1 + "Turn. ", 15, 30 + 9*displayHeight/10);
          }
        }
        click7 -=1;
      }
      else if (click7 == 0)
      {
        controlg();
        click7 += 1;
        click1 = 1;
        click2 = 1;
        click3 = 1;
        click4 = 1;
        click5 = 1;
        click6 = 1;
        //click7 = 1;
        click8 = 1;
        click9 = 1;
        click10 = 1;
        click11 = 1;
        click12 = 1;
      }
    }


    if ( mouseX > 305.9f* displayWidth/588 && mouseX < 361.9f* displayWidth/588 && mouseY > 9* displayHeight/16 && mouseY < 11* displayHeight/16)
    {
      if (click8 == 1)
      {

        fill(205, 133, 63);
        rect(displayWidth/5.5f+ (3* displayWidth/8.6f), 3*(displayHeight/4), displayWidth/18, displayHeight/8);
        //rect(displayWidth/5.5+ (0* displayWidth/8.6), displayHeight/8, displayWidth/18, displayHeight/8);
        hole.set(7, hole.get(7)+1);
        c1 += 1;
        println("Counter 1 = " + c1);
        println("a = " + hole.get(7) );
        pushStyle();
        textSize(30);
        fill(0);
        text(hole.get(7), 10+ displayWidth/5.5f + 3*displayWidth/8.6f, 40 + 6*displayHeight/8);
        popStyle();
        // lower rect for hints
        if (hole.get(7) == 1 && c1 == c2)
        {
          if (turn == "PLAYER 1")
          {
            fill(205, 133, 63);
            rect(displayWidth/3, 0, 2*(displayWidth/3), displayHeight/10);
            //rect(0, 9*(displayHeight/10), 2*(displayWidth/3), displayHeight/10);
            fill(0);
            textSize(30);
            text("It's " + turn2 + " Turn. ", 15 + displayWidth/3, 30);
            turn = turn2;
          }
          else if ( turn == "PLAYER 2")
          {
            fill(205, 133, 63);
            rect(0, 9*(displayHeight/10), 2*(displayWidth/3), displayHeight/10);
            fill(0);
            textSize(30);
            text("It's " + turn1 + "Turn. ", 15, 30 + 9*displayHeight/10);
          }
        }
        click8 -=1;
      }
      else if (click8 == 0)
      {
        controlh();
        click8 += 1;
        click1 = 1;
        click2 = 1;
        click3 = 1;
        click4 = 1;
        click5 = 1;
        click6 = 1;
        click7 = 1;
        //click8 = 1;
        click9 = 1;
        click10 = 1;
        click11 = 1;
        click12 = 1;
      }
    }

    if ( mouseX > 368.9f* displayWidth/588 && mouseX < 424.9f* displayWidth/588 && mouseY > 5.2f* displayHeight/16 && mouseY < 7.2f* displayHeight/16)
    {
      if (click9 == 1)
      {

        fill(205, 133, 63);
        rect(displayWidth/5.5f+ (4* displayWidth/8.6f), displayHeight/8, displayWidth/18, displayHeight/8);
        hole.set(8, hole.get(8)+1);
        c1 += 1;
        println("Counter 1 = " + c1);
        println("a = " + hole.get(8) );
        pushStyle();
        textSize(30);
        fill(0);
        text(hole.get(8), 10+ displayWidth/5.5f + 4*displayWidth/8.6f, 40 + displayHeight/8);
        popStyle();
        // lower rect for hints
        if (hole.get(8) == 1 && c1 == c2)
        {
          if (turn == "PLAYER 1")
          {
            fill(205, 133, 63);
            rect(displayWidth/3, 0, 2*(displayWidth/3), displayHeight/10);
            //rect(0, 9*(displayHeight/10), 2*(displayWidth/3), displayHeight/10);
            fill(0);
            textSize(30);
            text("It's " + turn2 + " Turn. ", 15 + displayWidth/3, 30);
            turn = turn2;
          }
          else if ( turn == "PLAYER 2")
          {
            fill(205, 133, 63);
            rect(0, 9*(displayHeight/10), 2*(displayWidth/3), displayHeight/10);
            fill(0);
            textSize(30);
            text("It's " + turn1 + "Turn. ", 15, 30 + 9*displayHeight/10);
          }
        }
        click9 -= 1;
      }
      else if (click9 == 0)
      {
        controli();
        click9 += 1;
        click1 = 1;
        click2 = 1;
        click3 = 1;
        click4 = 1;
        click5 = 1;
        click6 = 1;
        click7 = 1;
        click8 = 1;
        //click9 = 1;
        click10 = 1;
        click11 = 1;
        click12 = 1;
      }
    }


    if ( mouseX > 368.9f* displayWidth/588 && mouseX < 424.9f* displayWidth/588 && mouseY > 9* displayHeight/16 && mouseY < 11* displayHeight/16)
    {
      if (click10 == 1)
      {   

        fill(205, 133, 63);
        rect(displayWidth/5.5f+ (4* displayWidth/8.6f), 3*(displayHeight/4), displayWidth/18, displayHeight/8);
        //rect(displayWidth/5.5+ (0* displayWidth/8.6), displayHeight/8, displayWidth/18, displayHeight/8);
        hole.set(9, hole.get(9)+1);
        c1 += 1;
        println("Counter 1 = " + c1);
        println("a = " + hole.get(9) );
        pushStyle();
        textSize(30);
        fill(0);
        text(hole.get(9), 10+ displayWidth/5.5f + 4*displayWidth/8.6f, 40 + 6*displayHeight/8);
        popStyle();
        // lower rect for hints
        if (hole.get(9) == 1 && c1 == c2)
        {
          if (turn == "PLAYER 1")
          {
            fill(205, 133, 63);
            rect(displayWidth/3, 0, 2*(displayWidth/3), displayHeight/10);
            //rect(0, 9*(displayHeight/10), 2*(displayWidth/3), displayHeight/10);
            fill(0);
            textSize(30);
            text("It's " + turn2 + " Turn. ", 15 + displayWidth/3, 30);
            turn = turn2;
          }
          else if ( turn == "PLAYER 2")
          {
            fill(205, 133, 63);
            rect(0, 9*(displayHeight/10), 2*(displayWidth/3), displayHeight/10);
            fill(0);
            textSize(30);
            text("It's " + turn1 + "Turn. ", 15, 30 + 9*displayHeight/10);
          }
        }
        click10 -= 1;
      }
      else if (click10 == 0 )
      {
        controlj();
        click10 += 1;
        click1 = 1;
        click2 = 1;
        click3 = 1;
        click4 = 1;
        click5 = 1;
        click6 = 1;
        click7 = 1;
        click8 = 1;
        click9 = 1;
        //click10 = 1;
        click11 = 1;
        click12 = 1;
      }
    }


    if ( mouseX > 31*displayWidth/42 && mouseX < 5*displayWidth/6 && mouseY > 5.2f* displayHeight/16 && mouseY < 7.2f* displayHeight/16)
    {
      if (click11 == 1)
      {
        fill(205, 133, 63);
        rect(displayWidth/5.5f+ (5* displayWidth/8.6f), displayHeight/8, displayWidth/18, displayHeight/8);
        hole.set(10, hole.get(10)+1);
        c1 += 1;
        println("Counter 1 = " + c1);
        println("a = " + hole.get(10) );
        pushStyle();
        textSize(30);
        fill(0);
        text(hole.get(10), 10+ displayWidth/5.5f + 5*displayWidth/8.6f, 40 + displayHeight/8);
        popStyle();
        // lower rect for hints
        if (hole.get(10) == 1 && c1 == c2)
        {
          if (turn == "PLAYER 1")
          {
            fill(205, 133, 63);
            rect(displayWidth/3, 0, 2*(displayWidth/3), displayHeight/10);
            //rect(0, 9*(displayHeight/10), 2*(displayWidth/3), displayHeight/10);
            fill(0);
            textSize(30);
            text("It's " + turn2 + " Turn. ", 15 + displayWidth/3, 30);
            turn = turn2;
          }
          else if ( turn == "PLAYER 2")
          {
            fill(205, 133, 63);
            rect(0, 9*(displayHeight/10), 2*(displayWidth/3), displayHeight/10);
            fill(0);
            textSize(30);
            text("It's " + turn1 + "Turn. ", 15, 30 + 9*displayHeight/10);
          }
        }
        click11 -= 1;
      }
      else if (click11 == 0 )
      {
        controlk();
        click11 += 1;
        click1 = 1;
        click2 = 1;
        click3 = 1;
        click4 = 1;
        click5 = 1;
        click6 = 1;
        click7 = 1;
        click8 = 1;
        click9 = 1;
        click10 = 1;
        //click11 = 1;
        click12 = 1;
      }
    }

    if ( mouseX > 429* displayWidth/588 && mouseX < 486* displayWidth/588 && mouseY > 9* displayHeight/16 && mouseY < 11* displayHeight/16)
    {
      if (click12 == 1)
      {


        fill(205, 133, 63);
        rect(displayWidth/5.5f+ (5* displayWidth/8.6f), 3*(displayHeight/4), displayWidth/18, displayHeight/8);
        //rect(displayWidth/5.5+ (0* displayWidth/8.6), displayHeight/8, displayWidth/18, displayHeight/8);
        hole.set(11, hole.get(11)+1);
        c1 += 1;
        println("Counter 1 = " + c1);
        println("a = " + hole.get(11) );
        pushStyle();
        textSize(30);
        fill(0);
        text(hole.get(11), 10+ displayWidth/5.5f + 5*displayWidth/8.6f, 40 + 3*displayHeight/4);
        popStyle();
        // lower rect for hints
        if (hole.get(11) == 1 && c1 == c2)
        {
          if (turn == "PLAYER 1")
          {
            fill(205, 133, 63);
            rect(displayWidth/3, 0, 2*(displayWidth/3), displayHeight/10);
            //rect(0, 9*(displayHeight/10), 2*(displayWidth/3), displayHeight/10);
            fill(0);
            textSize(30);
            text("It's " + turn2 + " Turn. ", 15 + displayWidth/3, 30);
            turn = turn2;
          }
          else if ( turn == "PLAYER 2")
          {
            fill(205, 133, 63);
            rect(0, 9*(displayHeight/10), 2*(displayWidth/3), displayHeight/10);
            fill(0);
            textSize(30);
            text("It's " + turn1 + "Turn. ", 15, 30 + 9*displayHeight/10);
          }
        }
        click12 -= 1;
      }
      else if (click12 == 0)
      {
        controll();
        click12 += 1;
        click1 = 1;
        click2 = 1;
        click3 = 1;
        click4 = 1;
        click5 = 1;
        click6 = 1;
        click7 = 1;
        click8 = 1;
        click9 = 1;
        click10 = 1;
        click11 = 1;
        // click12 = 1;
      }
    }
  }



  //Carrying for Hole a
  //if (mouseX > displayWidth/5.5 + 0* displayWidth/8.6 && mouseX < displayWidth/5.5 + 0* displayWidth + displayWidth/18 && mouseY > displayHeight/8 && mouseY < displayHeight/4)
  public void controla()
  {
    c1 = 0;
    pushStyle();
    fill(205, 133, 63);
    rect(0, 9*displayHeight/10, 2*displayWidth/3, displayHeight/10);
    rect(displayWidth/5.5f + 0* displayWidth/8.6f, displayHeight/8, displayWidth/18, displayHeight/8);
    rect(displayWidth/3, 0, 2* displayWidth/3, displayHeight/10);
    image(ayo_img[3], 7* displayWidth/48, 10+displayHeight/4, displayWidth/8, displayHeight/4);
    //rect((displayWidth/6) - 20, displayHeight/4, displayWidth/8, displayHeight/4);
    popStyle();

    pushStyle();
    fill(0);
    textSize(15);
    text("Now, distribute the " + hole.get(0) + " stones by puttting a stone in each successive", 5+displayWidth/3, 15);
    text("pocket in the anti-clockwise direction.", 5 + displayWidth/3, 35);
    c2 = hole.get(0);
    println("Counter 2 " + c2);
    hole.set(0, 0);
    textSize(30);
    text(hole.get(0), 10+ displayWidth/5.5f + 0*displayWidth/8.6f, 40 + displayHeight/8);
    popStyle();
  }

  //Carrying for Hole b
  //else if (mouseX > displayWidth/5.5 + 0* displayWidth/8.6 && mouseX < displayWidth/5.5 + 0* displayWidth + displayWidth/18 &&  mouseY > 3*displayHeight/4 && mouseY < 7*displayHeight/8)
  public void controlb()
  {
    c1 = 0;
    pushStyle();
    fill(206, 133, 63);
    rect(displayWidth/3, 0, 2* displayWidth/3, displayHeight/10);
    rect(displayWidth/5.5f + 0*displayWidth/8.6f, 3*displayHeight/4, displayWidth/18, displayHeight/8);
    rect(0, 9*displayHeight/10, 2*displayWidth/3, displayHeight/10);
    image(ayo_img[3], 7*displayWidth/48, 5+displayHeight/2, displayWidth/8, displayHeight/4);
    popStyle();

    pushStyle();
    fill(0);
    textSize(15);
    text("Now, distribute the " + hole.get(1) + " stones by puttting a stone in each successive", 5, 15+9*displayHeight/10);
    text("pocket in the anti-clockwise direction.", 5, 35+9*displayHeight/10);
    c2 = hole.get(1);
    println(c2);
    hole.set(1, 0);
    textSize(30);
    text(hole.get(1), 10+displayWidth/5.5f+ 0*displayWidth/8.6f, 40+ 6*displayHeight/8);
    popStyle();
  }



  //Carrying for Hole c
  //    if (mouseX > displayWidth/5.5 + 1* displayWidth/8.6 && mouseX < displayWidth/5.5 + 1* displayWidth + displayWidth/18 && mouseY > displayHeight/8 && mouseY < displayHeight/4)
  public void controlc()
  {
    c1 = 0;
    pushStyle();
    fill(205, 133, 63);
    rect(0, 9*displayHeight/10, 2*displayWidth/3, displayHeight/10);
    rect(displayWidth/5.5f + 1* displayWidth/8.6f, displayHeight/8, displayWidth/18, displayHeight/8);
    rect(displayWidth/3, 0, 2* displayWidth/3, displayHeight/10);
    image(ayo_img[3], 13* displayWidth/48, 10+displayHeight/4, displayWidth/8, displayHeight/4);
    //rect((displayWidth/6) - 20, displayHeight/4, displayWidth/8, displayHeight/4);
    popStyle();

    pushStyle();
    fill(0);
    textSize(15);
    text("Now, distribute the " + hole.get(2) + " stones by puttting a stone in each successive", 5+displayWidth/3, 15);
    text("pocket in the anti-clockwise direction.", 5 + displayWidth/3, 35);
    c2 = hole.get(2);
    println("Counter 2 " + c2);
    hole.set(2, 0);
    textSize(30);
    text(hole.get(2), 10+ displayWidth/5.5f + 1*displayWidth/8.6f, 40 + displayHeight/8);
    popStyle();
  }

  //Carrying for Hole d
  //else if (mouseX > displayWidth/5.5 + 1* displayWidth/8.6 && mouseX < displayWidth/5.5 + 1* displayWidth + displayWidth/18 && mouseY > 3*displayHeight/4 && mouseY < 7*displayHeight/8)
  public void controld()
  {
    c1 = 0;
    pushStyle();
    fill(206, 133, 63);
    rect(displayWidth/3, 0, 2* displayWidth/3, displayHeight/10);
    rect(displayWidth/5.5f + 1*displayWidth/8.6f, 3*displayHeight/4, displayWidth/18, displayHeight/8);
    rect(0, 9*displayHeight/10, 2*displayWidth/3, displayHeight/10);
    image(ayo_img[3], 12.8f*displayWidth/48, 5+displayHeight/2, displayWidth/8, displayHeight/4);
    popStyle();

    pushStyle();
    fill(0);
    textSize(15);
    text("Now, distribute the " + hole.get(3) + " stones by puttting a stone in each successive", 5, 15+9*displayHeight/10);
    text("pocket in the anti-clockwise direction.", 5, 35+9*displayHeight/10);        
    c2 = hole.get(3);
    println(c2);
    hole.set(3, 0);
    textSize(30);
    text(hole.get(3), 10+displayWidth/5.5f+ 1*displayWidth/8.6f, 40+ 6*displayHeight/8);
    popStyle();
  }

  //Carrying for Hole e
  //  if (mouseX > displayWidth/5.5 + 2* displayWidth/8.6 && mouseX < displayWidth/5.5 + 2* displayWidth + displayWidth/18 && mouseY > displayHeight/8 && mouseY < displayHeight/4)
  public void controle()
  {
    c1 = 0;
    pushStyle();
    fill(205, 133, 63);
    rect(0, 9*displayHeight/10, 2*displayWidth/3, displayHeight/10);
    rect((displayWidth/5.5f + 2* displayWidth/8.6f), displayHeight/8, displayWidth/18, displayHeight/8);
    rect(displayWidth/3, 0, 2* displayWidth/3, displayHeight/10);
    image(ayo_img[3], 18.5f* displayWidth/48, 10+displayHeight/4, displayWidth/8.8f, displayHeight/4);
    //rect((displayWidth/6) - 20, displayHeight/4, displayWidth/8, displayHeight/4);
    popStyle();

    pushStyle();
    fill(0);
    textSize(15);
    text("Now, distribute the " + hole.get(4) + " stones by puttting a stone in each successive", 5+displayWidth/3, 15);
    text("pocket in the anti-clockwise direction.", 5 + displayWidth/3, 35);
    c2 = hole.get(4);
    println("Counter 2 " + c2);
    hole.set(4, 0);
    textSize(30);
    text(hole.get(4), 10+ displayWidth/5.5f + 2*displayWidth/8.6f, 40 + displayHeight/8);
    popStyle();
  }

  //Carrying for Hole f
  //else if (mouseX > displayWidth/5.5 + 2* displayWidth/8.6 && mouseX < displayWidth/5.5 + 2* displayWidth + displayWidth/18 && mouseY > 3*displayHeight/4 && mouseY < 7*displayHeight/8)
  public void controlf()
  {
    c1 = 0;
    pushStyle();
    fill(206, 133, 63);
    rect(displayWidth/3, 0, 2* displayWidth/3, displayHeight/10);
    rect(displayWidth/5.5f + 2*displayWidth/8.6f, 3*displayHeight/4, displayWidth/18, displayHeight/8);
    rect(0, 9*displayHeight/10, 2*displayWidth/3, displayHeight/10);
    image(ayo_img[3], 18.4f*displayWidth/48, 5+displayHeight/2, displayWidth/8.8f, displayHeight/4);
    popStyle();

    pushStyle();
    fill(0);
    textSize(15);
    text("Now, distribute the " + hole.get(5) + " stones by puttting a stone in each successive", 5, 15+9*displayHeight/10);
    text("pocket in the anti-clockwise direction.", 5, 35+9*displayHeight/10);
    c2 = hole.get(5);
    println(c2);
    hole.set(5, 0);
    textSize(30);
    text(hole.get(5), 10+displayWidth/5.5f+ 2*displayWidth/8.6f, 40+ 6*displayHeight/8);
    popStyle();
  }


  //Carrying for Hole g
  //  if (mouseX > displayWidth/5.5 + 3* displayWidth/8.6 && mouseX < displayWidth/5.5 + 3* displayWidth + displayWidth/18 && mouseY > displayHeight/8 && mouseY < displayHeight/4)
  public void controlg()
  {
    c1 = 0;
    pushStyle();
    fill(205, 133, 63);
    rect(0, 9*displayHeight/10, 2*displayWidth/3, displayHeight/10);
    rect(displayWidth/5.5f + 3* displayWidth/8.6f, displayHeight/8, displayWidth/18, displayHeight/8);
    rect(displayWidth/3, 0, 2* displayWidth/3, displayHeight/10);
    image(ayo_img[3], 24.2f* displayWidth/48, 10+displayHeight/4, displayWidth/8.8f, displayHeight/4);
    //rect((displayWidth/6) - 20, displayHeight/4, displayWidth/8, displayHeight/4);
    popStyle();

    pushStyle();
    fill(0);
    textSize(15);
    text("Now, distribute the " + hole.get(6) + " stones by puttting a stone in each successive", 5+displayWidth/3, 15);
    text("pocket in the anti-clockwise direction.", 5 + displayWidth/3, 35);
    c2 = hole.get(6);
    println("Counter 2 " + c2);
    hole.set(6, 0);
    textSize(30);
    text(hole.get(6), 10+ displayWidth/5.5f + 3*displayWidth/8.6f, 40 + displayHeight/8);
    popStyle();
  }

  //Carrying for Hole h
  //else if (mouseX > displayWidth/5.5 + 3* displayWidth/8.6 && mouseX < displayWidth/5.5 + 3* displayWidth + displayWidth/18 && mouseY > 3*displayHeight/4 && mouseY < 7*displayHeight/8)
  public void controlh()
  {
    c1 = 0;
    pushStyle();
    fill(206, 133, 63);
    rect(displayWidth/3, 0, 2* displayWidth/3, displayHeight/10);
    rect(displayWidth/5.5f + 3*displayWidth/8.6f, 3*displayHeight/4, displayWidth/18, displayHeight/8);
    rect(0, 9*displayHeight/10, 2*displayWidth/3, displayHeight/10);
    image(ayo_img[3], 24.1f*displayWidth/48, 5+displayHeight/2, displayWidth/8.8f, displayHeight/4);
    popStyle();

    pushStyle();
    fill(0);
    textSize(15);
    text("Now, distribute the " + hole.get(7) + " stones by puttting a stone in each successive", 5, 15+9*displayHeight/10);
    text("pocket in the anti-clockwise direction.", 5, 35+9*displayHeight/10);
    c2 = hole.get(7);
    println(c2);
    hole.set(7, 0);
    textSize(30);
    text(hole.get(7), 10+displayWidth/5.5f+ 3*displayWidth/8.6f, 40+ 6*displayHeight/8);
    popStyle();
  }



  //Carrying for Hole i
  //  if (mouseX > displayWidth/5.5 + 4* displayWidth/8.6 && mouseX < displayWidth/5.5 + 4* displayWidth + displayWidth/18 && mouseY > displayHeight/8 && mouseY < displayHeight/4)
  public void controli()
  {
    c1 = 0;
    pushStyle();
    fill(205, 133, 63);
    rect(0, 9*displayHeight/10, 2*displayWidth/3, displayHeight/10);
    rect(displayWidth/5.5f + 4* displayWidth/8.6f, displayHeight/8, displayWidth/18, displayHeight/8);
    rect(displayWidth/3, 0, 2* displayWidth/3, displayHeight/10);
    image(ayo_img[3], 29.8f* displayWidth/48, 10+displayHeight/4, displayWidth/8.8f, displayHeight/4);
    //rect((displayWidth/6) - 20, displayHeight/4, displayWidth/8, displayHeight/4);
    popStyle();

    pushStyle();
    fill(0);
    textSize(15);
    text("Now, distribute the " + hole.get(8) + " stones by puttting a stone in each successive", 5+displayWidth/3, 15);
    text("pocket in the anti-clockwise direction.", 5 + displayWidth/3, 35);
    c2 = hole.get(8);
    println("Counter 2 " + c2);
    hole.set(8, 0);
    textSize(30);
    text(hole.get(8), 10+ displayWidth/5.5f + 4*displayWidth/8.6f, 40 + displayHeight/8);
    popStyle();
  }

  //Carrying for Hole j
  //else if (mouseX > displayWidth/5.5 + 4* displayWidth/8.6 && mouseX < displayWidth/5.5 + 4* displayWidth + displayWidth/18 && mouseY > 3*displayHeight/4 && mouseY < 7*displayHeight/8)
  public void controlj()
  {
    c1 = 0;
    pushStyle();
    fill(206, 133, 63);
    rect(displayWidth/3, 0, 2* displayWidth/3, displayHeight/10);
    rect(displayWidth/5.5f + 4*displayWidth/8.6f, 3*displayHeight/4, displayWidth/18, displayHeight/8);
    rect(0, 9*displayHeight/10, 2*displayWidth/3, displayHeight/10);
    image(ayo_img[3], 29.7f*displayWidth/48, 5+displayHeight/2, displayWidth/8.8f, displayHeight/4);
    popStyle();

    pushStyle();
    fill(0);
    textSize(15);
    text("Now, distribute the " + hole.get(9) + " stones by puttting a stone in each successive", 5, 15+9*displayHeight/10);
    text("pocket in the anti-clockwise direction.", 5, 35+9*displayHeight/10);
    c2 = hole.get(9);
    println(c2);
    hole.set(9, 0);
    textSize(30);
    text(hole.get(9), 10+displayWidth/5.5f+ 4*displayWidth/8.6f, 40+ 6*displayHeight/8);
    popStyle();
  }


  //Carrying for Hole k
  //  if (mouseX > displayWidth/5.5 + 5* displayWidth/8.6 && mouseX < displayWidth/5.5 + 5* displayWidth + displayWidth/18 && mouseY > displayHeight/8 && mouseY < displayHeight/4  )
  public void controlk()
  {


    c1 = 0;
    pushStyle();
    fill(205, 133, 63);
    rect(0, 9*displayHeight/10, 2*displayWidth/3, displayHeight/10);
    rect(displayWidth/5.5f + 5* displayWidth/8.6f, displayHeight/8, displayWidth/18, displayHeight/8);
    rect(displayWidth/3, 0, 2* displayWidth/3, displayHeight/10);
    image(ayo_img[3], 35.4f* displayWidth/48, 10+displayHeight/4, displayWidth/8.8f, displayHeight/4);
    //rect((displayWidth/6) - 20, displayHeight/4, displayWidth/8, displayHeight/4);
    popStyle();

    pushStyle();
    fill(0);
    textSize(15);
    text("Now, distribute the " + hole.get(10) + " stones by puttting a stone in each successive", 5, 15+9*displayHeight/10);
    text("pocket in the anti-clockwise direction.", 5, 35+9*displayHeight/10);
    c2 = hole.get(10);
    println("Counter 2 " + c2);
    hole.set(10, 0);
    textSize(30);
    text(hole.get(10), 10+ displayWidth/5.5f + 5*displayWidth/8.6f, 40 + displayHeight/8);
    popStyle();
  }

  //Carrying for Hole l
  //else if (mouseX > displayWidth/5.5 + 5* displayWidth/8.6 && mouseX < displayWidth/5.5 + 5* displayWidth + displayWidth/18 && mouseY > 3*displayHeight/4 && mouseY < 7*displayHeight/8)
  public void controll()
  {
    c1 = 0;
    pushStyle();
    fill(206, 133, 63);
    rect(displayWidth/3, 0, 2* displayWidth/3, displayHeight/10);
    rect(displayWidth/5.5f + 5*displayWidth/8.6f, 3*displayHeight/4, displayWidth/18, displayHeight/8);
    rect(0, 9*displayHeight/10, 2*displayWidth/3, displayHeight/10);
    image(ayo_img[3], 35.3f*displayWidth/48, 5+displayHeight/2, displayWidth/8.8f, displayHeight/4);
    popStyle();

    pushStyle();
    fill(0);
    textSize(15);
    text("Now, distribute the " + hole.get(11) + " stones by puttting a stone in each successive", 5, 15+9*displayHeight/10);
    text("pocket in the anti-clockwise direction.", 5, 35+9*displayHeight/10);
    c2 = hole.get(11);
    println(c2);
    hole.set(11, 0);
    textSize(30);
    text(hole.get(11), 10+displayWidth/5.5f+ 5*displayWidth/8.6f, 40+ 6*displayHeight/8);
    popStyle();
  }




  public void control()
  {
    // for banks
    if (mousePressed)
    {
      if (mouseX > displayWidth/ 28 && mouseX < 3*displayWidth/28 && mouseY > displayHeight/4 && mouseY < 3*displayHeight/8)
      {    
        P1 += 4;
        pushStyle();
        fill(0);
        textSize(35);
        image(ayo_img[3], 0, 3*displayHeight/8, displayWidth/7, displayHeight/4);
        text( " X" + P1, 20+ displayWidth/68, 8* displayHeight/16);
        textSize(30);
        text(P1, 10+ displayWidth/28, 40+displayHeight/4);
        popStyle(); 

        pushStyle();
        fill(206, 133, 63);
        noStroke();
        rect(displayWidth/28, displayHeight/4, displayWidth/14, displayHeight/8);
        image(ayo_img[2], displayWidth/45, 8*displayHeight/18, 20, 20);
        popStyle();
      }
      else if (mouseX > 25*displayWidth/28 && mouseX < 55* displayWidth/56 && mouseY > displayHeight/4 && mouseY < 3*displayHeight/8)
      {    
        P2 += 4;
        pushStyle();
        fill(0);
        noStroke();
        textSize(35);
        image(ayo_img[3], 6*(displayWidth/7), 3*(displayHeight/8), displayWidth/7, displayHeight/4);
        text( "X" + P2, 20+ 60*displayWidth/68, 8* displayHeight/16);
        textSize(30);
        text(P2, 10+ 25*displayWidth/28, 40+displayHeight/4);
        popStyle();

        pushStyle();
        fill(206, 133, 63);
        noStroke();
        rect(25*displayWidth/28, displayHeight/4, displayWidth/14, displayHeight/8);
        image(ayo_img[2], 39*displayWidth/45, 8*displayHeight/18, 20, 20);
        popStyle();
      }
    }
  }
}

public void reset() {
  nave = new Nave(200, width/2);
  misiles = new ArrayList<Misil>();
  velocidad = 1;
  vidas = 3;
  puntos = 0;
  contBonus = 0;
  atractor = 0;
  tokenVida = false;
  tokenAtractor = false;
}


public void tablero() {
  fill(255);
  textSize(30);
  text(puntos, 10, 40);

  fill(255, 0, 0);
  text(contBonus, 10, height-10);

  for (int i=0; i<vidas; i++) {
    fill(255, 0, 0);
    ellipse(20+i*20, 60, 10, 10);
  }
}

public void sp_draw()
{
  if (frameCount % 5 == 0) {
    misiles.add(new Misil(width+10, random(height), 0));
  }
  if (frameCount % 60 == 0) {
    misiles.add(new Misil(width+10, random(height), 1)); //bonus
  }
  if (tokenVida) {
    misiles.add(new Misil(width+10, random(height), 2)); //vida
    tokenVida = false;
  }
  if (tokenAtractor) {
    misiles.add(new Misil(width+10, random(height), 3)); //atractor
    tokenAtractor = false;
  }

  if (vidas > 0) {
    nave.draw();

    for (int i=0; i < misiles.size(); i++) {
      Misil tal = misiles.get(i);
      tal.ataque();
      tal.draw();

      int t = nave.verChoque(tal);
      switch(t) {
      case 0 : //misil
        if (atractor == 0) {
          vidas--;
          contBonus = 0;
          nave.reset();
        }
        misiles.remove(i);
        break;
      case 1 : //bonus
        puntos = puntos + bonus;
        contBonus++; 
        if (contBonus % 2 == 0) tokenVida = true;
        if (contBonus % 5 == 0) tokenAtractor = true;
        misiles.remove(i);
        break;
      case 2 : //vida
        vidas++;
        misiles.remove(i);
        break;
      case 3 : //atractor
        atractor += 1000;
        misiles.remove(i);
        break;
      }

      if (atractor>0) {
        if (tal.getTipo()==1) {
          tal.atractor(nave, 1); //entre nave y bonuses
          atractor--;
        }
      }

      if (tal.paso()) {
        misiles.remove(i);
      }
    }
  }
  else reset();

  tablero();

  if (second() % 60 < 5)
    velocidad += 0.005f;

  println(velocidad);
}

//Mem Memory;
////PWidgetContainer Wid_Con;
////PButton memory;
//
//
//void setup()
//{
//  orientation(PORTRAIT);
//  size(displayWidth,displayHeight);
//  //Wid_Con = new PWidgetContainer(this);
//  //memory = new PButton(displayWidth/3,5*(displayHeight/6),displayWidth/3,displayHeight/6, "Memory");
//  
//  Memory = new Mem(int(displayWidth/5), int(displayHeight/5)); 
//  Memory.display();
//}
//void draw()
//{
//  //Wid_Con.addWidget(memory);
//  
//}
//
//void mousePressed()
//{
//  Memory.click();
//  //background(0);
//}
//
///*void onClickWidget(PWidget widget) //extends Mem
//{
//  if (widget == memory)
//  {
//    Memory.display();
//  }
//}*/
//







public class Mem //extends PWidgetContainer
{

  int w, h, moves, counter, holder, holder_r, holder_c;
  IntList numlist, exposed, img_b, img_c ;
  PImage[] bcks = new PImage[5];
  PImage[] card = new PImage[2];
  PImage ova;
  //PImage card_back1, card_back2, Mem_Back;


  //constructor
  Mem(int W, int H) 
  {

    //card_back1 = loadImage("card_back1.jpg");
    //card_back2 = loadImage("card_back2.jpg");
    //Mem_Back = loadImage("bckgrnd.jpg");
    for (int i = 0 ; i < bcks.length; i++)
    {
      bcks[i] = loadImage("bck"+str(i+2)+".png");
    }
    for (int i = 0; i <card.length; i++)
    {
      card[i] = loadImage("card_back"+str(i+1)+".jpg");
    }
    ova = loadImage("mem.png");
    w = W;
    h = H;
    moves = 0;

    numlist = new IntList();
    exposed = new IntList();
    img_b = new IntList();
    img_c = new IntList();
    counter = 0;
    holder = 0;
    holder_r = 0;
    holder_c = 0;

    for (int i= 0; i < 8 ; i++)
    {
      numlist.append(i);
      println(numlist);
    }
    for ( int I = 7 ; I>=0 ; I--)
    {
      numlist.append(I);
      println(numlist);
    }

    for (int i = 0; i < 16; i++)
    {
      exposed.append(8);
    }

    numlist.shuffle();
    println(numlist);
    for (int i = 0 ; i <5; i++)
    {
      img_b.append(i);
    }
    img_b.shuffle();
    for (int i = 0 ; i < 2 ; i++)
    {
      img_c.append(i);
    }
    img_c.shuffle();
  }
  // display function

  public void display()
  {

    image(bcks[img_b.get(0)], 0, 0, displayWidth, displayHeight);

    for (int r = 0; r < 4 ; r++)
    {
      for (int c = 0 ; c < 4  ; c ++) //&& for (int i = 0; i < 16; i ++)
      {
        pushStyle();
        textAlign(CENTER);
        textSize(50);
        fill(0);
        text( str(numlist.get((4*r)+c)), 60+(displayWidth/4)*r, 20 + displayHeight/4 + (displayHeight/6)*c ); 
        popStyle();


        pushStyle();
        noFill();
        stroke(255);
        rect ((displayWidth/4 )* r, displayHeight/6 + (displayHeight/6)*c, displayWidth/4, displayHeight/6);
        image(card[img_c.get(0)], (displayWidth/4 )* r, displayHeight/6 + (displayHeight/6)*c, displayWidth/4, displayHeight/6); 
        popStyle();
      }
    }

    pushStyle();
    fill(255);
    rect(displayWidth/20, displayHeight/30, displayWidth/3, displayHeight/20);
    popStyle();
    pushStyle();
    textSize(20);
    fill(0);
    text("MOVES: ", 5+ displayWidth/20, 20+displayHeight/30); 
    popStyle();

//    rect(displayWidth/3, 6*(displayHeight/7), displayWidth/3, displayHeight/10);
//    pushStyle();
//    fill(0);
//    textSize(20);
//    text("RESTART", 5 + displayWidth/3, 20+ 6*(displayHeight/7), displayWidth/3, displayHeight/10);
//    popStyle();
  }

  public void Restart()
  {
    display();
  }



  public void click()
  {
    /*if (mousePressed == true)
     { 
     float x = 0;
     x = constrain(x,displayWidth/3, 2* (displayWidth/3));
     float y = 0; 
     y = constrain(y, 6*(displayHeight/7), displayHeight/10);
     if ( mouseX == x && mouseY == y)
     {
     Restart();
     }
     
     }*/
    for ( int i = 0 ; i < 4 ; i++)
    {
      if (mouseX > 0 && mouseX < displayWidth/4)
      {
        if (mouseY > ( displayHeight/6 + (displayHeight/6)*i)  && mouseY < (displayHeight/6 + (displayHeight/6)*(1+i)))
        { 
          if (mousePressed == true)
          {
            if (counter == 0)
            {

              holder = numlist.get(i);
              holder_c = i;
              holder_r = 0;
              println("HR" + holder_r);
              println("H  " + holder);
              counter = 1;
              println("Counter" + counter);
              exposed.set(i, numlist.get(i));
              println("exposed" + exposed);


              pushStyle();
              textAlign(CENTER);
              textSize(50);
              fill(200, 133, 100);
              text( str(numlist.get(i)), 60+(displayWidth/4)*0, 20 + displayHeight/4 + (displayHeight/6)*i ); 
              popStyle();
            }
            else if (counter == 1)
            {
              moves = moves + 1;
              rect(displayWidth/20, displayHeight/30, displayWidth/3, displayHeight/20);
              pushStyle();
              fill(0);
              textSize(20);
              text("MOVES =  " + str(moves), 5+ displayWidth/20, 20+displayHeight/30);
              popStyle();
              //rect(displayWidth/20, displayHeight/30, displayWidth/3, displayHeight/30);
              counter = 0;
              println("Counter"+counter);
              exposed.set(i, numlist.get(i));
              println("exposed" + exposed);

              if (numlist.get(i) == holder)
              {
                pushStyle();
                textAlign(CENTER);
                textSize(50);
                fill(200, 133, 100);
                text( str(numlist.get(i)), 60+(displayWidth/4)*0, 20 + displayHeight/4 + (displayHeight/6)*i ); 
                popStyle();
              }
              else
              {
                pushStyle();
                noFill();
                stroke(255);
                rect ((displayWidth/4 )* 0, displayHeight/6 + (displayHeight/6)*i, displayWidth/4, displayHeight/6);
                image(card[img_c.get(0)], (displayWidth/4 )* 0, displayHeight/6 + (displayHeight/6)*i, displayWidth/4, displayHeight/6);
                popStyle();
                pushStyle();
                noFill();
                stroke(255);
                rect ((displayWidth/4 )* holder_r, displayHeight/6 + (displayHeight/6)*holder_c, displayWidth/4, displayHeight/6);
                image(card[img_c.get(0)], (displayWidth/4 )* holder_r, displayHeight/6 + (displayHeight/6)*holder_c, displayWidth/4, displayHeight/6);
                popStyle();
              }
            }

            //background(0);
          }
        }
      }
      else if (mouseX > displayWidth/4 && mouseX < (displayWidth/2))
      {
        if (mouseY > ( displayHeight/6 + (displayHeight/6)*i)  && mouseY < (displayHeight/6 + (displayHeight/6)*(1+i)))
        {
          int col = 1;
          if (mousePressed == true)
          {
            if (counter == 0)
            {

              holder = numlist.get((4*col)+i);
              holder_c = i;
              holder_r = 1;
              println("HR " + holder_r);
              println("H  " + holder);
              counter = 1;
              println("Counter" + counter);
              exposed.set((4*col)+i, numlist.get((4*col)+i));
              println("exposed" + exposed);


              pushStyle();
              textAlign(CENTER);
              textSize(50);
              fill(200, 133, 100);
              text( str(numlist.get((4*col)+i)), 60+(displayWidth/4)*col, 20 + displayHeight/4 + (displayHeight/6)*i ); 
              popStyle();
            }
            else if (counter == 1)
            {
              moves = moves + 1;
              rect(displayWidth/20, displayHeight/30, displayWidth/3, displayHeight/20);
              pushStyle();
              fill(0);
              textSize(20);
              text("MOVES =  " + str(moves), 5+ displayWidth/20, 20+displayHeight/30);
              popStyle();
              counter = 0;
              println("Counter"+counter);
              exposed.set((4*col)+i, numlist.get((4*col)+i));
              println("exposed" + exposed);

              if (numlist.get((4*col)+i) == holder)
              {
                pushStyle();
                textAlign(CENTER);
                textSize(50);
                fill(200, 133, 100);
                text( str(numlist.get((4*col)+i)), 60+(displayWidth/4)*col, 20 + displayHeight/4 + (displayHeight/6)*i ); 
                popStyle();
              }
              else
              {
                pushStyle();
                noFill();
                stroke(255);
                rect ((displayWidth/4 )* 1, displayHeight/6 + (displayHeight/6)*i, displayWidth/4, displayHeight/6);
                image(card[img_c.get(0)], (displayWidth/4 )* 1, displayHeight/6 + (displayHeight/6)*i, displayWidth/4, displayHeight/6);
                popStyle();
                pushStyle();
                noFill();
                stroke(255);
                rect ((displayWidth/4 )* holder_r, displayHeight/6 + (displayHeight/6)*holder_c, displayWidth/4, displayHeight/6);
                image(card[img_c.get(0)], (displayWidth/4 )* holder_r, displayHeight/6 + (displayHeight/6)*holder_c, displayWidth/4, displayHeight/6);
                popStyle();
              }
            }
          }
        }
      }
      else if (mouseX > displayWidth/2 && mouseX < 3*(displayWidth/4))
      {
        if (mouseY > ( displayHeight/6 + (displayHeight/6)*i)  && mouseY < (displayHeight/6 + (displayHeight/6)*(1+i)))
        { 
          int col = 2;
          if (mousePressed == true)
          {
            if (counter == 0)
            {

              holder = numlist.get((4*col)+i);
              holder_c = i;
              holder_r = 2;
              println("HR " + holder_r);
              println("H  " + holder);
              counter = 1;
              println("Counter" + counter);
              exposed.set((4*col)+i, numlist.get((4*col)+i));
              println("exposed" + exposed);

              pushStyle();
              textAlign(CENTER);
              textSize(50);
              fill(200, 133, 100);
              text( str(numlist.get((4*col)+i)), 60+(displayWidth/4)*col, 20 + displayHeight/4 + (displayHeight/6)*i ); 
              popStyle();
            }

            //background(0);
            else if (counter == 1)
            {
              moves = moves + 1;
              rect(displayWidth/20, displayHeight/30, displayWidth/3, displayHeight/20);
              pushStyle();
              fill(0);
              textSize(20);
              text("MOVES =  " + str(moves), 5+ displayWidth/20, 20+displayHeight/30);
              popStyle();
              counter = 0;
              println("Counter"+counter);
              exposed.set((4*col)+i, numlist.get((4*col)+i));
              println("exposed" + exposed);

              if (numlist.get((4*col)+i) == holder)
              {
                pushStyle();
                textAlign(CENTER);
                textSize(50);
                fill(200, 133, 100);
                text( str(numlist.get((4*col)+i)), 60+(displayWidth/4)*col, 20 + displayHeight/4 + (displayHeight/6)*i ); 
                popStyle();
              }
              else
              {
                pushStyle();
                noFill();
                stroke(255);
                rect ((displayWidth/4 )* 1, displayHeight/6 + (displayHeight/6)*i, displayWidth/4, displayHeight/6);
                image(card[img_c.get(0)], (displayWidth/4 )* 1, displayHeight/6 + (displayHeight/6)*i, displayWidth/4, displayHeight/6);
                popStyle();
                pushStyle();
                noFill();
                stroke(255);
                rect ((displayWidth/4 )* holder_r, displayHeight/6 + (displayHeight/6)*holder_c, displayWidth/4, displayHeight/6);
                image(card[img_c.get(0)], (displayWidth/4 )* holder_r, displayHeight/6 + (displayHeight/6)*holder_c, displayWidth/4, displayHeight/6);
                popStyle();
              }
            }
          }
        }
      }

      else if (mouseX > 3*(displayWidth/4) && mouseX < displayWidth)
      {
        if (mouseY > ( displayHeight/6 + (displayHeight/6)*i)  && mouseY < (displayHeight/6 + (displayHeight/6)*(1+i)))
        { 
          int col = 3;
          if (mousePressed == true)
          {
            if (counter == 0)
            {

              holder = numlist.get((4*col)+i);
              holder_c = i;
              holder_r = 3;
              println("HR " + holder_r);
              println("H  " + holder);
              counter = 1;
              println("Counter" + counter);
              exposed.set((4*col)+i, numlist.get((4*col)+i));
              println("exposed" + exposed);

              pushStyle();
              textAlign(CENTER);
              textSize(50);
              fill(200, 133, 100);
              text( str(numlist.get((4*col)+i)), 60+(displayWidth/4)*col, 20 + displayHeight/4 + (displayHeight/6)*i ); 
              popStyle();
            }

            else if (counter == 1)
            {
              moves = moves + 1;
              rect(displayWidth/20, displayHeight/30, displayWidth/3, displayHeight/20);
              pushStyle();
              fill(0);
              textSize(20);
              text("MOVES =  " + str(moves), 5+ displayWidth/20, 20+displayHeight/30);
              popStyle();
              counter = 0;
              println("Counter"+counter);
              exposed.set((4*col)+i, numlist.get((4*col)+i));
              println("exposed" + exposed);

              if (numlist.get((4*col)+i) == holder)
              {
                pushStyle();
                textAlign(CENTER);
                textSize(50);
                fill(200, 133, 100);
                text( str(numlist.get((4*col)+i)), 60+(displayWidth/4)*col, 20 + displayHeight/4 + (displayHeight/6)*i ); 
                popStyle();
              }
              else
              {
                pushStyle();
                noFill();
                stroke(255);
                rect ((displayWidth/4 )* 1, displayHeight/6 + (displayHeight/6)*i, displayWidth/4, displayHeight/6);
                image(card[img_c.get(0)], (displayWidth/4 )* 1, displayHeight/6 + (displayHeight/6)*i, displayWidth/4, displayHeight/6);
                popStyle();
                pushStyle();
                noFill();
                stroke(255);
                rect ((displayWidth/4 )* holder_r, displayHeight/6 + (displayHeight/6)*holder_c, displayWidth/4, displayHeight/6);
                image(card[img_c.get(0)], (displayWidth/4 )* holder_r, displayHeight/6 + (displayHeight/6)*holder_c, displayWidth/4, displayHeight/6);
                popStyle();
              }
            }
          }
        }
      }
    }


    if (exposed.hasValue(8) == false)
    {
      println(" Game Over");
      //image(bck4,0,0,displayWidth,displayHeight);
      image(ova,0,displayHeight/4,displayWidth,displayHeight/4);
    }
  }
}

class Misil {
  float x;
  float y;
  //float vel = 1;
  int tam;
  int tipo;
  int tiempoAtractor = 200;

  Misil(float x_, float y_, int tipo_) {
    x = x_;
    y = y_;
    tipo = tipo_;
    if (tipo == 2 || tipo == 3) 
      tam = 20; 
    else tam = 10;
  }

  public void ataque() {
    if ( !paso() ) { 
      x -= velocidad;
    }
  }

  public void atractor(Nave n, int tipo_) {
    if (tipo == tipo_) {
      x = lerp(x, n.getX()+100, 0.03f);
      y = lerp(y, n.getY(), 0.03f);
    }
  }

  public boolean paso() {
    if (x<-2*tam) 
      return true;
    else 
      return false;
  }

  public void draw() {
    switch(tipo) {
    case 0 : //misil
      noStroke();
      fill(125,125,50);
      rect(x, y, tam, tam);
      break;
    case 1 : //bonus
      noStroke();
      fill(0, 150, 200);
      rect(x, y, tam, tam);
      break;
    case 2 : //points
      noStroke();
      fill(255, 100, 0);
      rect(x, y, tam, tam);
      fill(255);
      textSize(24);
      text("\u2665", x, y+tam);
      break;
    case 3: //atractor
      float c = map(sin(frameCount/10.0f), -1, 1, 0, 255);
      fill(c, 0, 0);
      rect(x, y, tam, tam);
      fill(0, 0, c);
      textSize(30);
      text("A", x, y+tam);
    }
  }  

  //GETTERS Y SETTERS
  public float getX() {
    return x;
  }
  public float getY() {
    return y;
  }
  public int getTam() {
    return tam;
  }
  public int getTipo() {
    return tipo;
  }
}




APMediaPlayer player1;
APMediaPlayer player2;
class Nave {
  float x;
  float y;
  float vel = 2;
  boolean choco;
  int tam = 10;
  PImage ship;
  PImage back1, back2;

  //  
  //    player1 = new APMediaPlayer(this);
  //   player1.setMediaFile("soundtrack1.mp3");
  //   player2 = new APMediaPlayer(this);
  //   player2.setMediaFile("soundtrack2.mp3");

  Nave(float x_, float y_) {
    x = x_;
    y = y_;
    choco = false;
    ship = loadImage("ship.png");
    back1 = loadImage("nebula_blue.png");
    back2 = loadImage("debris.png");
  }

  public int verChoque(Misil m) {
    PVector p[] = new PVector[4];
    p[0] = new PVector(x, y);
    p[1] = new PVector(x, y+tam);
    p[2] = new PVector(x+tam, y);
    p[3] = new PVector(x+tam, y+tam);

    for (int i=0; i<4; i++) {
      if (p[i].x > m.getX() && p[i].x < m.getX()+m.getTam() &&
        p[i].y > m.getY() && p[i].y < m.getY()+m.getTam()) {
        if (m.getTipo() == 0) choco = true;
        return m.getTipo();
      }
    }
    return -1;
  }


  public void reset() {
    choco = false;
  }

  public boolean choco() {
    return choco;
  }

  public void draw() {
    image(back1, 0, 0, displayWidth, displayHeight);
    image(back2, 0, 0, displayWidth, displayHeight);

    y = mouseY;
    image(ship, x, y, tam*4, tam*4);

    if (atractor>0) {
      noFill();
      float t = map(sin(frameCount/10.0f), -1, 1, 4*tam, 10*tam);
      stroke(0, 255, 0);
      ellipse(x+tam/2, y+tam/2, t, t);
    }
  }

  //GETTERS AND SETTERS
  public float getX() {
    return x;
  }
  public float getY() {
    return y;
  }
}


}
